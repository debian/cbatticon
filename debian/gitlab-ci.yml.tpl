include: https://salsa.debian.org/salsa-ci-team/pipeline/raw/master/salsa-ci.yml

# end of salsa pipeline bot parser

run piuparts:
  script:
    - echo "piuparts disabled. 3rd party ttf and fonts packages are leaving tons of files after purge. See https://salsa.debian.org/debian/FeedReader/-/jobs/33355"
